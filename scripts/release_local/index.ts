import { execa } from 'execa';
import simpleGit from 'simple-git';
import { hideBin } from 'yargs/helpers';
import yargs from 'yargs/yargs';
import { buildAllPackages } from '../build/build-all-packages';
import { getMantinePackagesList } from '../packages/get-packages-list';
import { publishPackage } from '../publish/publish-package';
import { createLogger } from '../utils/signale';

const logger = createLogger('release');
const git = simpleGit();

const { argv }: { argv: any } = yargs(hideBin(process.argv))
  .option('stage', {
    type: 'string',
    choices: ['alpha', 'beta'],
    description: "Prerelease stage: 'alpha', 'beta'",
  })
  .option('tag', {
    type: 'string',
    default: 'latest',
    description: 'Tag',
  });

async function release() {
  const status = await git.status();

  if (status.files.length !== 0) {
    logger.error('Working tree is not clean');
    process.exit(1);
  }

  logger.log('Releasing all packages');

  await buildAllPackages();
  logger.success('All packages have been built successfully');

  logger.log('Publishing packages to npm');

  if (argv.stage && argv.tag === 'latest') {
    argv.tag = 'next';
  }

  const mantinePackages = await getMantinePackagesList();

  await Promise.all(
    mantinePackages.map((p) =>
      publishPackage({ packagePath: p!.path, name: p!.packageJson.name!, tag: argv.tag })
    )
  );

  logger.success('All packages have been published successfully');

  await execa('yarn');
}

release();
